import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getBooks(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/books`);
  }

  addBook(bookId: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/books/add`, {bookId: bookId});
  }

  getFilms(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/films`);
  }

  addFilm(filmId: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/films/add`, {bookId: filmId});
  }
}
