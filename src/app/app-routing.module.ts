import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {HomeComponent} from "./home/home.component";
import {AuthGuard} from "./guard/auth.guard"; // CLI imports router

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {
    path: 'espace',
    component: HomeComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        canActivateChild: [AuthGuard],
        children: [

        ]
      }
    ]
  },
  {path: '', redirectTo: '/espace', pathMatch: 'full'},
  {path: '*', redirectTo: '/espace', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
