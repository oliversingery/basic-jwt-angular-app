import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../service/auth.service";
import {Router} from "@angular/router";
import {TokenStorageService} from "../service/token-storage.service";
import {ErrorComponent} from "../error/error.component";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild(ErrorComponent)
  loginError: ErrorComponent;
  form: FormGroup;

  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router, private token: TokenStorageService) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login() {
    const val = this.form.value;

    if (val.email && val.password) {
      this.auth.login(val.email, val.password)
        .subscribe(
          (data) => {
            this.token.saveToken(data.token);
            this.token.saveUser({username: val.username});
            this.router.navigateByUrl('/');
          },
          (error) => {
            switch (error.status) {
              case 401: {
                this.loginError.show('Incorrect credentials'); break;
              }
              default: {
                this.loginError.show('An unknown error occured. Please try again later'); break;
              }
            }

          });
    }
  }

}
