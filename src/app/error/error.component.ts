import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  active = false;
  errorMsg: string;

  constructor() { }

  ngOnInit(): void {
  }

  show(msg: string) {
    this.errorMsg = msg;
    this.active = true;
  }

  hide() {
    this.active = false;
  }


}
